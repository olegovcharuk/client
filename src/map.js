'use strict';

let L = require('leaflet');
const colors = [
    'red',
    'blue',
    'green',
    'pink',
    'brown',
    'yellow',
    'black',
    'grey',
    'orange',
    'magenta',
    'cyan'
];

function LMap(div) {
    document.getElementById(div).setAttribute("style", "border: 2px solid lightgray; border-radius: 3px;");
    this.map = L.map(div).setView([62.549048, 77.381047], 4);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoidmd2b2xlZyIsImEiOiJjamR1cjNsM2MxaDV0MnhwY2J0bHgyemxxIn0.uPAdtzDKaDnWQEDE2NU_7g', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets-basic'
    }).addTo(this.map);

    this.stationIcon = L.icon({
        iconUrl: '../static/train.png',
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 32], // point of the icon which will correspond to marker's location
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    this.transferIcon = L.icon({
        iconUrl: '../static/refresh.png',
        iconSize:     [32, 32], // size of the icon
        iconAnchor:   [16, 32], // point of the icon which will correspond to marker's location
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    this.stations = new Map();
    this.markers = new Map();
    this.polylines = [];
}

LMap.prototype.clearMarkers = function () {
    let that = this;

    this.markers.forEach(marker => {
        that.map.removeLayer(marker);
    });
    this.markers.clear();

    this.polylines.forEach(polyline => {
        that.map.removeLayer(polyline);
    });
    this.polylines = [];
};

LMap.prototype.init = function (data) {
    let that = this;
    that.stations.clear();
    data.forEach(station => {
        that.stations.set(station.key, station);
    });
    this.addMarkers(data);
};

LMap.prototype.navigateTo = function (key) {
    let station = this.stations.get(key);
    this.map.flyTo([station.latitude, station.longitude]);
};

LMap.prototype.addMarkers = function (data) {
    let that = this;
    that.clearMarkers();

    if (data[0]) {
        that.map.setView([data[0].latitude, data[0].longitude], 8);
    }
    let prevPoint = [];
    let colorInd = 0;
    let prevDirection = (data[0] || {}).direction || "";
    data.forEach(function (station, index) {
        let icon = that.stationIcon;
        let coords = [station.latitude, station.longitude];
        if (index === 0) {
            prevPoint = coords;
        }

        let polylinePath = [coords, prevPoint];
        let color = colors[colorInd];
        let polyline = L.polyline(polylinePath, {color: color}).addTo(that.map);
        that.polylines.push(polyline);
        prevPoint = coords;
        if (station.direction !== prevDirection && index !== 1) {
            colorInd = colorInd === colors.length - 1 ? 0 : colorInd + 1;
            icon = that.transferIcon;
        }
        prevDirection = station.direction;

        let marker = L.marker(coords, {icon: icon}).addTo(that.map);
        marker.bindTooltip(station.title, {
            direction: 'top',
            sticky: true
        });
        that.markers.set(station.key, marker);
    });
};

module.exports = LMap;
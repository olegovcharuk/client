'use strict';

let $ = require('jquery');
let spinjs = require('spin.js');
let autocomplete = require('javascript-autocomplete');
let LMap = require('./map');
let StationsList = require('./stations-list');
const host = "http://212.237.54.217";

function Application() {
    this.SERVER_URL = host + ":24530";
    this.loadingDiv = document.getElementById("loading");
    this.contentDiv = document.getElementById("content");
    this.cities = [];
    this.spinner = null;
    this.stationsList = null;
    this.map = null;
}

Application.prototype.setUp = function () {
    let that = this;

    $.ajaxSetup({
        beforeSend: function () {
            that.contentDiv.setAttribute("style", "opacity: 0.8");
            that.spinner = new spinjs.Spinner().spin(that.loadingDiv);
        },
        complete: function () {
            that.spinner.stop();
            that.contentDiv.setAttribute("style", "opacity: 1");

        }
    });

    let onAutoComplete = function (term, suggest) {
        term = term.toLowerCase();
        let matches = [];
        for (let i = 0; i < that.cities.length; i++)
            if (~that.cities[i].toLowerCase().indexOf(term)) matches.push(that.cities[i]);
        suggest(matches);
    };

    new autocomplete({
        selector: '#from',
        minChars: 2,
        source: onAutoComplete
    });

    new autocomplete({
        selector: '#to',
        minChars: 2,
        source: onAutoComplete
    });

    $("#submitButton").on('click', function () {
        $(".header").addClass("top");
        let fromInput = $("#from");
        let toInput = $("#to");

        let from = $.trim(fromInput.val());
        let to = $.trim(toInput.val());

        if (from.length === 0 || to.length === 0) {
            throw Error("nenene brat ya tak ne rabotayu");
        }
        that.getRoute(from, to);
    });
};


Application.prototype.start = function () {
    this.setUp();
    this.loadCitiesList();
};

Application.prototype.createList = function (data) {
    let that = this;
    if (!this.stationsList) {
        this.stationsList = new StationsList('list', function (stationKey) {
            if (that.map) {
                that.map.navigateTo(stationKey);
            }
        });
    }
    this.stationsList.init(data);
};

Application.prototype.createMap = function (data) {
    if (!this.map) {
        this.map = new LMap('mapid');
    }
    this.map.init(data);
};

Application.prototype.getRoute = function (from, to) {
    let that = this;

    $.ajax({
        type: "GET",
        url: that.SERVER_URL + "/route/from/" + from + "/to/" + to,
        success: function (data) {
            that.createMap(data);
            that.createList(data);
        },
        error: function () {
            that.createMap(data);
            that.createList(data);
        }
    });
};

Application.prototype.loadCitiesList = function () {
    let that = this;

    $.ajax({
        type: "GET",
        url: that.SERVER_URL + "/settlement/all/",
        success: function (data) {
            data.forEach(city => {
                that.cities.push(city.name);
            });
        },
        error: function () {
        }
    });
};

new Application().start();

// TODO: delete
$("#from").val("Нижний Новгород");
$("#to").val("Москва");


function StationsList(divId, onClickToCard) {
    this.div = document.getElementById(divId);
    this.onClickToCard = onClickToCard;
}

function createStationCard(station, onClickToCard) {
    let cardDiv = document.createElement('div');
    cardDiv.className = "station-card";

    let stationTitle = document.createElement('div');
    stationTitle.className = "station-title";
    stationTitle.innerHTML = station.title;
    cardDiv.appendChild(stationTitle);

    let stationPosition = document.createElement('div');
    stationPosition.className = "station-position";
    let settlement = station.settlement === "" ? "" : ", " + station.settlement;
    stationPosition.innerHTML = station.region + settlement;
    cardDiv.appendChild(stationPosition);

    cardDiv.addEventListener('click', function () {
        onClickToCard(station.key);
    });

    return cardDiv;
}

function createTransferCard() {
    let transferDiv = document.createElement('div');
    transferDiv.className = "transfer-card";
    transferDiv.innerHTML = 'ПЕРЕСАДКА';
    return transferDiv;
}

StationsList.prototype.init = function (data) {
    let that = this;
    let listDiv = this.div;
    while (listDiv.firstChild) {
        listDiv.removeChild(listDiv.firstChild);
    }
    document.getElementById('list-title').innerHTML = "Ваш путь:";

    if (data.length === 0) {
        let notFoundDiv = document.createElement('div');
        notFoundDiv.className = "not-found";
        notFoundDiv.innerHTML = "...не найден :(";
        listDiv.appendChild(notFoundDiv);
        return;
    }

    let previousDirection = data[0].direction || '';
    data.forEach((station, index) => {
        listDiv.appendChild(createStationCard(station, that.onClickToCard));
        if (station.direction !== previousDirection && index !== data.length - 1 && index !== 1) {
            listDiv.appendChild(createTransferCard());
        }
        previousDirection = station.direction;
    });
};

module.exports = StationsList;